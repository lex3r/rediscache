<?php
namespace Alex\Helpers;
use Redis;

/**
 * RedisCache
 */
class RedisCache
{
    private Redis $redis;

    public function __construct(string $host = "localhost", int $port = 6379)
    {
        $this->redis = new Redis();
        $this->redis->connect($host, $port);
    }

    public function read(string $key, callable $newDataQuery, bool $isString = false, int $expires = 0): array|string
    {
        $readData = $this->redis->get($key);
        if (!$readData) {
            $newData = $newDataQuery();
            if ($expires) {
                $this->redis->setex($key, $expires, $isString ? $newData : json_encode($newData));
            } else {
                $this->redis->set($key, $isString ? $newData : json_encode($newData));
            }
            return $newData;
        }
        return $isString ? $readData : json_decode($readData, true);
    }

    /**
     * @return Redis
     */
    public function getInstance(): Redis
    {
        return $this->redis;
    }

    public function clear(): bool|Redis
    {
        return $this->redis->flushAll();
    }
}
